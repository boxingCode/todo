import React from 'react';
import logo from './logo.svg';
import './App.css';

import SetDateTime from './component/SetDateTime';
import Navigation from './component/Navigation';

function App() {
  const [todoList, setTodoList] = React.useState(new Array);

  function addElementToList(task){
    setTodoList([...todoList, task]);
  }

  return (
    <div className="App">
      <header className="App-header">
        <Navigation todoList={todoList}/>
      </header>
      <main className="Main-header">
        <SetDateTime addElementToList={addElementToList}/>
      </main>
    </div>
  );
}

export default App;
