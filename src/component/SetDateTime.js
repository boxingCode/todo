import React from 'react';
import SetDate from './SetDate';
import SetTime from './SetTime';
import Modal from 'react-modal';
import SetClock from './SetClock';


export default function SetDateTime(props) {

  const [name, setName] = React.useState("");
  const [date, setDate] = React.useState("");
  const [time, setTime] = React.useState(0);
  const [showModal, setShowModal] = React.useState(false);

  function handleChange(newValue, type) {
    if (type === 'date')
      setDate(newValue);
    if (type === 'time')
      setTime(newValue);
  }

  function saveTodoTask() {
    props.addElementToList({
      taskName: name,
      taskTime: time,
      taskDate: date
    });
    setName("");
    setDate("");
    setTime(0);
  }

  function saveTaskName(event) {
    setName(event.target.value)
  }

  function handelSaveModal() {
    setShowModal(false);
  }

  function handleOpenModal() {
    setShowModal(true);
  }

  function handleCloseModal() {
    setDate("");
    setShowModal(false);
  }

  // We pass a callback to Child
  return (
    <div className="app-main-wrapper">
      <div class="cursor">
        <input type="text" placeholder="Type task name" onChange={(e) => saveTaskName(e)} value={name} ></input>
        <i></i>
      </div>

      <SetClock time={time} onChange={handleChange} onSave={saveTodoTask} nameValueLength={name.length} dateValueLength={date.length} />
      <div>
        <div onClick={handleOpenModal} className="trigger-modal-button trigger-modal-button-view-update">
          <div>Add Time</div>
          <div className="trigger-modal-button-icon"></div>  
        </div>
        <Modal isOpen={showModal} contentLabel="Set Time Schedule">
          <div className="time-date-modal-container">
            <SetTime time={time} onChange={handleChange} />
            <SetDate date={date} onChange={handleChange} />
          </div>

          <div className="button-container">
            <button onClick={handelSaveModal} className="set-date-button"> Set Date </button>
            <button onClick={handleCloseModal} className="close-modal-button button-shift-mobile">X</button>
          </div>
        </Modal>
      </div>

    </div>
  );
}
