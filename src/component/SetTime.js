import React from 'react';

export default function SetTime(props) {
  function handleChange(event) {
    // Here, we invoke the callback with the new value
    console.log("child")
    props.onChange(event.target.value, 'time');
  }

  return (
    <div className="set-time-wrapper set-time-wrapper-media">
      <div class="cursor">
        <input placeholder={props.time + " minutes"} onChange={handleChange}/>
        <i></i>
      </div>
      <div className="modal-calendar-icon"></div>
    </div>
  );
}