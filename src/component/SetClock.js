import React from 'react';
import { CircleSlider } from "react-circle-slider";

export default function SetClock(props) {

  function handleChange(event) {
    // Here, we invoke the callback with the new value
    props.onChange(event, 'time');
  }

  function saveTodoTask() {
    console.log("CLICKED FROM GREE TICK CHILD");
    props.onSave();
  }

  if (props.nameValueLength > 0 && props.dateValueLength > 0) {
    return (
      <div className="clock-wrapper">
        <div className="div-time-wrapper">

          <p className="margin-bottom-8px">{props.time}</p>

          <div className="clock-schedule-icon-wrapper margin-bottom-8px">
            <span className="icon clock-schedule-icon margin-top-8px"></span>
          </div>

          <div className="save-clock-schedule-icon-wrapper margin-left-5px" onClick={saveTodoTask}>
            <span className="icon save-clock-schedule-icon margin-top-3px" title="Save"></span>
          </div>
        </div>

        <div className="div-circular-slider-wrapper">
          <CircleSlider size="200" max="60" circleColor="red" progressColor="white" progressWidth="5" knobColor="red" knobRadius="10" value={props.time} onChange={handleChange} />
        </div>
      </div>
    );
  }
  else {
    return (
      <div className="clock-wrapper">
        <div className="div-time-wrapper div-time-wrapper-content">

          <div>
            <p className="margin-bottom-8px">{props.time} Minutes</p>
          </div>

          <div className="clock-schedule-icon-wrapper margin-bottom-8px">
            <span className="icon clock-schedule-icon margin-top-8px"></span>
          </div>

        </div>

        <div className="div-circular-slider-wrapper">
          <CircleSlider size="200" max="60" circleColor="red" progressColor="white" progressWidth="5" knobColor="red" knobRadius="10" value={props.time} onChange={handleChange} />
        </div>
      </div>
    );
  }


}