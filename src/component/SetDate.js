import React from 'react';
import Calendar from 'react-calendar';

export default function SetDate(props) {
  const todaysdate = new Date();
  function handleChange(event) {
    // Here, we invoke the callback with the new value
    console.log("child")
    console.log(`${event.getDate()} + ${event.getMonth()} + ${event.getFullYear()}`)
    props.onChange(`${event.getDate()} , ${event.getMonth()} , ${event.getFullYear()}`, 'date');
  }

  return (
    <div>
      <div className="set-date-wrapper">
        <p type="text">{props.date}</p>
      </div>
      <div className="react-calendar-wrapper">
        <Calendar onChange={(e) => handleChange(e)} minDate={todaysdate} />
      </div>
    </div>
  );

}