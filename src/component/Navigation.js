import React from 'react';
import SideDrawNavigation from './SideDrawNavigation';


export default function Navigation(props) {
  function handleChange(event) {
    // Here, we invoke the callback with the new value
    console.log("Navigation")
  }

  return (
    <div className="header-template">
      <div className="hamburger-name-container">
        <span className="navigation-icon hamberger-icon margin-left-13px" /><span className="margin-left-13px">Name</span>
      </div>
      <SideDrawNavigation todoList={props.todoList}></SideDrawNavigation>
    </div>
  );
}