import React from 'react';

export default function SideDrawNavigation(props) {

  function openNav() {
    document.getElementById("mySidenav").style.width = "100%";
  }

  function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }

  function collapseItem(event) {
    event.preventDefault();

    if (event.target.matches("span")) {
      bubbleUp(event);

    } else {
      parentClick(event);
    }
  }

  function bubbleUp(event) {
    let classlist = event.target.parentElement.querySelector('.expand-icon').classList;
    if (classlist.value.includes('expand-item-icon-up')) {
      event.target.parentElement.querySelector('.expand-icon').classList.remove('expand-item-icon-up');
      event.target.parentElement.querySelector('.expand-icon').classList.add('expand-item-icon-down')
    } else if (classlist.value.includes('expand-item-icon-down')) {
      event.target.parentElement.querySelector('.expand-icon').classList.remove('expand-item-icon-down');
      event.target.parentElement.querySelector('.expand-icon').classList.add('expand-item-icon-up')
    }

    var content = event.target.parentElement.nextSibling;
    if (content.style.display === "block") {
      content.style.display = "none";
    } else {
      content.style.display = "block";
    }
  }

  function parentClick(event) {
    let classlist = event.target.querySelector('.expand-icon').classList;

    if (classlist.value.includes('expand-item-icon-up')) {
      event.target.querySelector('.expand-icon').classList.remove('expand-item-icon-up');
      event.target.querySelector('.expand-icon').classList.add('expand-item-icon-down')
    } else if (classlist.value.includes('expand-item-icon-down')) {
      event.target.querySelector('.expand-icon').classList.remove('expand-item-icon-down');
      event.target.querySelector('.expand-icon').classList.add('expand-item-icon-up')
    }

    var content = event.target.nextSibling;
    if (content.style.display === "block") {
      content.style.display = "none";
    } else {
      content.style.display = "block";
    }
  }

  return (
    <div>
      <div className="margin-right-13px">
        <span className="navigation-icon notification-icon" onClick={openNav} />
      </div>
      <div id="mySidenav" className="side-navigation">
        <span className="closebtn" onClick={closeNav}></span>
        <span className="clock-icon"></span>
        {props.todoList.map((item, index) => {
          return (
            <div className="collapsable-container" key={item.taskName}>
              <div className="collapsible" onClick={(e) => collapseItem(e)}>
                <span>{item.taskName}</span>
                <span className="expand-item-icon-down expand-icon"></span>
              </div>
              <div className="content">
                <div className="content-wrapper">
                  <span>Task time: {item.taskTime} Minutes</span>
                  <span>Task date: {item.taskDate}</span>
                </div>
              </div>
            </div>
          )
        })}
      </div>

    </div>
  );
}